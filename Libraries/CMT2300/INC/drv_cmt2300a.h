#ifndef __DRV_CMT2300A_H
#define __DRV_CMT2300A_H

//#include "sys.h"
#include "typedefs.h"

#define CMT2300A_GPIO1_PORT						GPIOD
#define CMT2300A_GPIO1_PIN						GPIO_Pin_0

#define CMT2300A_GPIO2_PORT						GPIOD
#define CMT2300A_GPIO2_PIN						GPIO_Pin_1

#define CMT2300A_GPIO3_PORT						GPIOD
#define CMT2300A_GPIO3_PIN						GPIO_Pin_2

#define CMT2300A_SPI_CSB_PORT					GPIOA
#define CMT2300A_SPI_CSB_PIN					GPIO_Pin_4

#define CMT2300A_SPI_SCLK_PORT					GPIOA
#define CMT2300A_SPI_SCLK_PIN					GPIO_Pin_5

#define CMT2300A_SPI_MOSI_PORT					GPIOA
#define CMT2300A_SPI_MOSI_PIN					GPIO_Pin_7

#define CMT2300A_SPI_MISO_PORT					GPIOA
#define CMT2300A_SPI_MISO_PIN					GPIO_Pin_6

#define CMT2300A_SPI_FCSB_PORT					GPIOA
#define CMT2300A_SPI_FCSB_PIN					GPIO_Pin_3

uint8_t CMT2300A_ReadGpio1(void);
uint8_t CMT2300A_ReadGpio2(void);
uint8_t CMT2300A_ReadGpio3(void);

void CMT2300A_InitGpio(void);
u8 CMT2300A_ReadReg(u8 addr);
void CMT2300A_WriteReg(u8 addr, u8 dat);

void CMT2300A_ReadFifo(u8 buf[], u16 len);
void CMT2300A_WriteFifo(const u8 buf[], u16 len);

#endif


