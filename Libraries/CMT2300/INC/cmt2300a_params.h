#ifndef __CMT2300A_PARAMS_H
#define __CMT2300A_PARAMS_H

#include "typedefs.h"
#include "cmt2300a_defs.h"

/*******************************************************
;---------------------------------------
;  CMT2300A Configuration File
;  Generated by CMOSTEK RFPDK 1.51
;  2021.09.06 14:12
;---------------------------------------
; Mode                      = Advanced
; Part Number               = CMT2300A
; Frequency                 = 410.000 MHz
; Xtal Frequency            = 26.0000 MHz
; Demodulation              = FSK
; AGC                       = On
; Data Rate                 = 1.0 kbps
; Deviation                 = 20.0 kHz
; Tx Xtal Tol.              = 20 ppm
; Rx Xtal Tol.              = 20 ppm
; TRx Matching Network Type = 20 dBm
; Tx Power                  = +20 dBm
; Gaussian BT               = NA
; Bandwidth                 = Auto-Select kHz
; CDR Type                  = Counting
; CDR DR Range              = NA
; AFC                       = On
; AFC Method                = Auto-Select
; Data Representation       = 0:F-low 1:F-high
; Rx Duty-Cycle             = Off
; Tx Duty-Cycle             = Off
; Sleep Timer               = Off
; Sleep Time                = NA
; Rx Timer                  = Off
; Rx Time T1                = NA
; Rx Time T2                = NA
; Rx Exit State             = STBY
; Tx Exit State             = STBY
; SLP Mode                  = Disable
; RSSI Valid Source         = PJD
; PJD Window                = 8 Jumps
; LFOSC Calibration         = On
; Xtal Stable Time          = 155 us
; RSSI Compare TH           = NA
; Data Mode                 = Packet
; Whitening                 = Disable
; Whiten Type               = NA
; Whiten Seed Type          = NA
; Whiten Seed               = NA
; Manchester                = Disable
; Manchester Type           = NA
; FEC                       = Disable
; FEC Type                  = NA
; Tx Prefix Type            = 0
; Tx Packet Number          = 1
; Tx Packet Gap             = 32
; Packet Type               = Variable Length
; Node-Length Position      = First Node, then Length
; Payload Bit Order         = Start from msb
; Preamble Rx Size          = 2
; Preamble Tx Size          = 8
; Preamble Value            = 170
; Preamble Unit             = 8-bit
; Sync Size                 = 4-byte
; Sync Value                = 2863279572
; Sync Tolerance            = None
; Sync Manchester           = Disable
; Node ID Size              = NA
; Node ID Value             = NA
; Node ID Mode              = None
; Node ID Err Mask          = Disable
; Node ID Free              = Disable
; Payload Length            = 32
; CRC Options               = None
; CRC Seed                  = NA
; CRC Range                 = NA
; CRC Swap                  = NA
; CRC Bit Invert            = NA
; CRC Bit Order             = NA
; Dout Mute                 = Off
; Dout Adjust Mode          = Disable
; Dout Adjust Percentage    = NA
; Collision Detect          = Off
; Collision Detect Offset   = NA
; RSSI Detect Mode          = Always
; RSSI Filter Setting       = No Filtering
; RF Performance            = High
; LBD Threshold             = 2.4 V
; RSSI Offset               = 26
; RSSI Offset Sign          = 1
********************************************************/
/* [CMT Bank] */
const u8 g_cmt2300aCmtBank[CMT2300A_CMT_BANK_SIZE] = {
    0x00,
    0x66,
    0xEC,
    0x1D,		//0X03=power
    0xF0,
    0x80,
    0x14,
    0x08,
    0x91,
    0x02,
    0x02,
    0xD0,
};

/* [System Bank] */
const u8 g_cmt2300aSystemBank[CMT2300A_SYSTEM_BANK_SIZE] = {
    0xAE,
    0xE0,
    0x35,
    0x00,
    0x00,
    0xF4,
    0x10,
    0xE2,
    0x42,
    0x20,
    0x00,
    0x81,
};

/* [Frequency Bank] */
const u8 g_cmt2300aFrequencyBank[CMT2300A_FREQUENCY_BANK_SIZE] = {
    0x3F,
    0x29,
    0xED,
    0x11,
    0x3F,
    0x13,
    0x3B,
    0x61,
};

/* [Data Rate Bank]--空速*/
const u8 g_cmt2300aDataRateBank[CMT2300A_DATA_RATE_BANK_SIZE] = {
    0x15,
    0x0A,
    0x10,
    0xCC,
    0xDA,
    0xDE,
    0x0C,
    0x0A,
    0xDF,
    0x26,
    0x29,
    0x29,
    0xC0,
    0x90,
    0x65,
    0x53,
    0x08,
    0x00,
    0xB4,
    0x00,
    0x00,
    0x01,
    0x00,
    0x00,
};

/* [Baseband Bank] */
const u8 g_cmt2300aBasebandBank[CMT2300A_BASEBAND_BANK_SIZE] = {
    0x12,		//REG=38--RX报头长度 [7:3] = 0-31,[1:0]=数据处理模式Direct/Packket
    0x08,		//REG=39--TX报头长度_L
    0x00,		//REG=3A--TX报头长度_H  [15:0] = 0-65535
    0xAA,		//REG=3B--RX/TX 的 共用报头
    0x06,		//REG=3C--RX时sync容错bit数，Sync word长度
    0x00,		//REG=3D--Sync Value[7:0]
    0x00,		//REG=3E--Sync Value[15:8]
    0x00,		//REG=3F--Sync Value
    0x00,		//REG=40--Sync Value
    0xD4,		//REG=41--Sync Value
    0x2D,		//REG=42--Sync Value
    0xAA,		//REG=43--Sync Value
    0xAA,		//REG=44--Sync Value[63:56]
    0x01,		//REG=45--[6:4]=payload有效负荷的长度(Byte)[10:8](变长时不起作用),[2]=Node ID 和Length Byte 的位置(0-ID在前),[0]=包长类型[0-固定包长]
    0x1F,		//REG=46--payload有效负荷的长度(Byte)[7:0]
    0x00,		//REG=47--[3:2]=Node ID 长度(0-1Byte)，[1:0]=Node ID 检测模式(0不检测)
    0x00,		//REG=48--Node ID [7:0]
    0x00,		//REG=49--Node ID [15:8]
    0x00,		//REG=4A--Node ID [23:16]
    0x00,		//REG=4B--Node ID [31:24]
    0x00,		//REG=4C--[6]=FEC编解码使能,[5:0]=CRC 配置,[0]=CRC使能(1使能)
    0x00,		//REG=4D--CRC多项式的初始值[7:0]
    0x00,		//REG=4E--CRC多项式的初始值[15:8]
    0x60,		//REG=4F--CRC大小端顺序配置
    0xFF,		//REG=50--白化编解码多项式的种子[7:0]
    0x00,		//REG=51--[1:0]=
    0x00,		//REG=52--[7:0]TX模式重复发的包个数
    0x1F,		//REG=53--[7:0]=TX重复发包时，包与包之间的间隔symbol
    0x10,		//REG=54--[6:0]FIFO的填入阈值
};

/* [Tx Bank] */
const u8 g_cmt2300aTxBank[CMT2300A_TX_BANK_SIZE] = {
    0x50,		//REG=55--[2]=选择TX的数据来源(FIFO/GPIO),调制方式，FSK
    0x9A,		//0X56
    0x0C,		//0X57
    0x00,		//0X58
    0xA1,		//0X59=空速
    0xD0,		//0X5a=空速
    0x00,		//0X5B
    0x8A,		//0X5C=power
    0x18,		//0X5D=power
    0x3F,		//0X5E
    0x7F,		//0X5F
};

#endif
