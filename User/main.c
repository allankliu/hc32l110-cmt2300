/**
 * LED Blink Demo
 * 
 * Wiring:
 * 
 *   P01   -> TXD
 *   P02   -> RXD
 *   P34   -> + LED1 -  -> GND
 *   P35   -> + LED-LED/TX -  -> GND, High is lid
 *   P36   -> + LED-GRN/RX -  -> GND, High is lid
*/
#include "gpio.h"
//#include "uart.h"
#include "bsp_printf.h"
#include "spi.h"

volatile uint8_t u8RxData[16] = {0x00}, u8RxFlg = 0, u8RxPos = 0;

/* void RxIntCallback(void)
{
    u8RxData[u8RxPos++] = UART1_RxReceive();
    u8RxPos = u8RxPos % 16;
    u8RxFlg = 1;
}
 */

void SPI_Init(void)
{
    stc_spi_config_t  SPIConfig;

    CLK_EnablePeripheralClk(ClkPeripheralSpi);

    Gpio_SetFunc_SPI_CS_P14();
    Gpio_SetFunc_SPI_MISO_P23();
    Gpio_SetFunc_SPI_MOSI_P24();
    Gpio_SetFunc_SPI_CLK_P25();

    SPIConfig.bCPHA = SpiClockPhaseFirst;
    SPIConfig.bCPOL = SpiClockPolarityLow;
    // Turn off interrupts
    SPIConfig.bIrqEn = FALSE;
    // Master mode
    SPIConfig.bMasterMode = SpiMaster;
    // Clock source, PCLK/2
    SPIConfig.u8ClkDiv = SpiClkDiv2;
    // No callback
    SPIConfig.pfnIrqCb = NULL;
    Spi_Init(&SPIConfig);
}

int main(void) {
    uint16_t i = 0;
    /**
     * Set PCLK = HCLK = Clock source to 24MHz
     */    
    Clk_Init(ClkFreq24Mhz, ClkDiv1, ClkDiv1);
    // Enable peripheral clock
    CLK_EnablePeripheralClk(ClkPeripheralBaseTim);
    CLK_EnablePeripheralClk(ClkPeripheralGpio); // GPIO clock is required, equal to M0P_CLOCK->PERI_CLKEN_f.GPIO = 1;
    CLK_EnablePeripheralClk(ClkPeripheralUart1);

    /* 
    Set P01,P02 as UART1 TX,RX, or use P35,P36
        Gpio_SetFunc_UART1_TX_P35();
        Gpio_SetFunc_UART1_RX_P36();
    */
    //Gpio_SetFunc_UART1_TXD_P01();
    //Gpio_SetFunc_UART1_RXD_P02();
    // Config UART1
    //Uart1_TxRx_Init(115200, RxIntCallback);

    //Uart1_TxString("Please input string and return\r\n");
    Bsp_PrinfInit(115200);
    printf("UART initialization\r\n");

    SPI_Init();
    printf("SPI initialization\r\n");

    /**
     * Set P34 and P35 as output
    */
    /*
    Gpio_InitIOExt(3, 4, 
                    GpioDirOut, // Output
                    FALSE,      // No pull up
                    FALSE,      // No pull down
                    FALSE,      // No open drain
                    FALSE);     // High driver capability
    */
    Gpio_InitIOExt(3, 5, GpioDirOut, FALSE, FALSE, FALSE, FALSE);
    Gpio_InitIOExt(3, 6, GpioDirOut, FALSE, FALSE, FALSE, FALSE);

    //Gpio_InitIOExt(0, 3, GpioDirOut, FALSE, FALSE, FALSE, FALSE);
    //Gpio_InitIOExt(1, 5, GpioDirOut, FALSE, FALSE, FALSE, FALSE);
    //Gpio_InitIOExt(1, 4, GpioDirOut, FALSE, FALSE, FALSE, FALSE);
    //Gpio_InitIOExt(1, 3, GpioDirOut, FALSE, FALSE, FALSE, FALSE);
    //Gpio_InitIOExt(2, 4, GpioDirOut, FALSE, FALSE, FALSE, FALSE);
    //Gpio_InitIOExt(2, 5, GpioDirOut, FALSE, FALSE, FALSE, FALSE);
    //Gpio_InitIOExt(2, 6, GpioDirOut, FALSE, FALSE, FALSE, FALSE);
    //Gpio_InitIOExt(3, 2, GpioDirOut, FALSE, FALSE, FALSE, FALSE);
    //Gpio_InitIOExt(3, 3, GpioDirOut, FALSE, FALSE, FALSE, FALSE);


    // Toggle LED blink
    while (1)
    {
        //GPIO_SetPinOutHigh(3, 4);
        GPIO_SetPinOutHigh(3, 5);
        GPIO_SetPinOutLow(3, 6);
        
        //GPIO_SetPinOutHigh(0, 3);
        //GPIO_SetPinOutHigh(1, 5);
        //GPIO_SetPinOutHigh(1, 4);
        //GPIO_SetPinOutHigh(1, 3);
        //GPIO_SetPinOutHigh(2, 4);
        //GPIO_SetPinOutHigh(2, 5);
        //GPIO_SetPinOutHigh(2, 6);
        //GPIO_SetPinOutHigh(3, 2);
        //GPIO_SetPinOutHigh(3, 3);

        delay1ms(100);
        //Uart1_TxString("Please input string and return\r\n");
        printf("Please input string and return %d\r\n",i++);

        //GPIO_SetPinOutLow(3, 4);
        GPIO_SetPinOutLow(3, 5);
        GPIO_SetPinOutHigh(3, 6);

        //GPIO_SetPinOutLow(0, 3);
        //GPIO_SetPinOutLow(1, 5);
        //GPIO_SetPinOutLow(1, 4);
        //GPIO_SetPinOutLow(1, 3);
        //GPIO_SetPinOutLow(2, 4);
        //GPIO_SetPinOutLow(2, 5);
        //GPIO_SetPinOutLow(2, 6);
        //GPIO_SetPinOutLow(3, 2);
        //GPIO_SetPinOutLow(3, 3);

        delay1ms(100);
    }
}

